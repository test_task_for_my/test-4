<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/style.css">
	<title>Тестовое задание 4</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			<form class="form" method="post" action="" id="search">
				<div class="row">
					<div class="col-9">
						<input required class="form-control" type="text" value="" name="searchWord" id="searchWord" placeholder="Найти на яндекс">
					</div>
					<div class="col-3">
						<button class="btn btn-primary" type="submit">Найти</button>
					</div>
				</div>
			</form>
			<div id="listResult" class="list-group"></div>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/common.js"></script>
</body>
</html>
