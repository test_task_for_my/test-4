<?php
/**
 * Created by PhpStorm.
 * Author: Taras Kovalev Victorovich
 * Email: taraswww777@mail.ru
 * Date: 25.11.2017 13:48
 */

/**
 * Парсер сайта Avito
 *
 * @property int  $pageSize  20 50 100 (рекомендуется по 20, нагрузка на бд ниже но так дольше)
 * @property bool $force     true - полностью пройтись повсем лотам
 */
class ParserYa extends Parser{

	protected $parser_code = 'ya';
	protected $force = true;
	protected $countPages = 0;
	protected $countRows = 0;
	protected $pageSize = 20;// 20 50 100
	protected $isVip = false;

	public function __construct(){
		parent::__construct();
	}

	public function run($word = ''){
		// параметр lr указывает на регион для которого делается поиск 15 это тула
		$url = 'https://yandex.ru/search/?text=' . urlencode($word).'&lr=15';
		$content = $this->cUrl($url);
		//exit($content);


		//$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/1.html');

		// избавимся от всех style и script
		$pattern = '#<style[^>]*?>(.+?)</style>#su';
		$content = preg_replace($pattern, '', $content);
		$pattern = '#<script[^>]*?>(.+?)</script>#su';
		$content = preg_replace($pattern, '', $content);
		// получим ul.serp-list
		$matchesDescription = null;
		// очень грубо получить список снипетов
		/*$pattern = '#<ul[^>]+?class\s*?=\s*?(["\'])serp-list serp-list_left_yes\1[^>]*?>(.+?)</ul>#su';preg_match_all($pattern, $content, $matches);*/


		// грубо получить список снипетов
		/*$pattern = '#<div[^>]+?class\s*?=\s*?(["\'])organic[^>]*?\1[^>]*?>(.*?)<li#su';preg_match_all($pattern, $content, $matches);*/

		$matchesHeaders = null;
		// получаем заголовки
		$pattern = '#<a[^>]+?class\s*?=\s*?(["\'])link link_theme_normal organic__url link_cropped_no i-bem[^>]*?\1[^>]*?>(.*?)</a>#su';
		preg_match_all($pattern, $content, $matchesHeaders);

		// получаем описания
		$pattern = '#<div[^>]+?class\s*?=\s*?(["\'])text-container[^>]*?\1[^>]*?>(.*?)</div>#su';
		preg_match_all($pattern, $content, $matchesDescription);

		// Оставляем только нуждные данные (это только для удобства)
		$listLinks = $matchesHeaders[0];
		$listTitles = $matchesHeaders[2];
		$listDescriptions = $matchesDescription[2];

		// резулитирующий массив
		$resData = [];
		// зачищаем от html тегов
		if( ! empty($listDescriptions)) foreach($listDescriptions as $k => $item){
			$resData[$k]['description'] = strip_tags($item);
		}
		if( ! empty($listTitles)) foreach($listTitles as $k => $item){
			$resData[$k]['title'] = strip_tags($item);
		}
		// получим ссылки
		if( ! empty($listLinks)) foreach($listLinks as $k => $item){
			$matches = null;
			$pattern = '#href\s*?=\s*?(["\'])(.*?)[^>]*?\1#';
			preg_match($pattern, $item, $matches);
			$resData[$k]['href'] = trim(trim($matches[0], 'href='), '"');
		}

		$html = '';
		if( ! empty($resData)) foreach($resData as $item){
			$html .= '<div class="list-group-item"><div class="h3"><a href="' . $item['href'] . '" target="_blank" title="' . $item['title'] . '">' . $item['title'] . '</a></div><div class="text-justify">' . $item['description'] . '</div></div>';
		}


		return $html;
	}
}