<?php


abstract class Parser{

	protected $parser_url = '';
	protected $parser_id = '';
	protected $parser_name = '';
	protected $parser_code = '';
	protected $cookie_file = '';
	protected $cash_file = '';
	protected $max_pages = 10;
	protected $needValidRedirect = false;


	public function __construct(){
		$this->cookie_file = $_SERVER['DOCUMENT_ROOT'] . '/~cookie/cookie_' . $this->parser_code . '.txt';
		$this->cash_file = $_SERVER['DOCUMENT_ROOT'] . '/~cash';
		if( ! file_exists(dirname($this->cookie_file))) mkdir(dirname($this->cookie_file), 0755, true);
	}


	/**
	 * Сохраняет данные в кеш
	 */
	protected function setCash($code, $data){
		if( ! $code) return false;
		$path = $this->getPath($code);
		if( ! file_exists(dirname($path))) mkdir(dirname($path), 0755, true);
		return file_put_contents($path, json_encode($data));
	}


	private function getPath($code){
		$replace = [$this->parser_url => '', 'https://' => '', 'http://' => '', '//' => '', '?' => '-', '=' => '-', '.' => '-', '&' => '-',];
		//$path = self::PATH . '/' . str_replace(array_keys($replace), array_values($replace), $code) . '.json';
		//return $path;
		return $this->cash_file . '/' . str_replace(array_keys($replace), array_values($replace), $code) . '.json';
	}

	/**
	 * Вернёт данные из кеша при необходимости можно указать его возраст
	 */
	protected function getCash($code, $time = null){
		if( ! $code) return false;
		$path = $this->getPath($code);
		if( ! file_exists($path)) return false;
		if($time){
			$age = $_SERVER['REQUEST_TIME'] - filemtime($path);
			if($age > $time) return false;
		}
		return json_decode(file_get_contents($path), true);
	}

	/**
	 * Вернёт контент со страницы
	 */
	protected function getContent($path){
		if(empty($path)) return null;
		return file_get_contents($path);
	}


	protected function cUrl($url, $dataPost = [], $cookies = []){
		//usleep(1000);
		//if( ! filter_var($url, FILTER_VALIDATE_URL)) return null;
		if(empty($url)) return null;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // результат сохраняется в переменную
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);// следовать за редиректами
		//curl_setopt($ch, CURLOPT_HEADER, true);// получить заголовок

		// для https
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		if( ! empty($dataPost)){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
		}
		if( ! empty($cookies)){
			foreach($cookies as $key => $val){
				curl_setopt($ch, CURLOPT_COOKIE, $key . '=' . $val);
			}
		}else{
			// сессия
			//curl_setopt($ch, CURLOPT_COOKIESESSION, true);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file);// запись
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file);// чтение
		}
		$res = curl_exec($ch);
		curl_close($ch);
		return $res;
	}

	protected function getRedirect($url){
		if(empty($url)) return null;
		ob_start();
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		//curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_NOBODY, true);  // Неотображать текст самой страницы
		curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$to = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
		curl_close($ch);
		ob_clean();
		if($code != 200) return $to;
		return false;
	}

	/**
	 * удаляем лишние пробелы
	 */
	protected function minHtml($str = null){
		if(empty($str)) return null;
		return trim(preg_replace('/\s{2,}/', ' ', $str));
	}

	/**
	 * Вернёт все GET параметры из указанного url
	 * @url https://webew.ru/articles/743.webew
	 *
	 * @param string      $url
	 * @param null|string $key
	 *
	 * @return array|string
	 */
	protected function getGetParameters($url, $key = null){
		$res = [];
		preg_match('/^([^?]+)(\?.*?)?(#.*)?$/', $url, $matches);
		$str = (isset($matches[2])) ? $matches[2] : '';
		if(empty($str)) return '';
		$arr = explode('&', trim($str, '?'));
		if( ! empty($arr)) foreach($arr as $item){
			$temp = explode('=', $item);
			$res[$temp[0]] = $temp[1];
		}
		if( ! empty($res) and null !== $key) return $res[$key];else return $res;
	}

	/**
	 * Удалит все GET параметры из url
	 * @url https://webew.ru/articles/743.webew
	 *
	 * @return string
	 */
	protected function removeGetParameters($url){ // remove GET-parameters from URL
		return preg_replace('/^([^?]+)(\?.*?)?(#.*)?$/', '$1$3', $url);
	}

	/**
	 * Замена содержимого GET-параметров
	 * @url https://webew.ru/articles/743.webew
	 *
	 * @return string
	 */
	protected function substituteGetParameters($url, $varname, $value) // substitute get parameter
	{
		if(is_array($varname)){
			foreach($varname as $i => $n){
				$v = (is_array($value)) ? (isset($value[$i]) ? $value[$i] : null) : $value;
				$url = $this->substituteGetParameters($url, $n, $v);
			}
			return $url;
		}

		preg_match('/^([^?]+)(\?.*?)?(#.*)?$/', $url, $matches);
		$gp = (isset($matches[2])) ? $matches[2] : ''; // GET-parameters
		if( ! $gp) return $url;

		$pattern = "/([?&])$varname=.*?(?=&|#|\z)/";
		if(preg_match($pattern, $gp)){
			$substitution = ($value !== '') ? "\${1}$varname=" . preg_quote($value) : '';
			$newgp = preg_replace($pattern, $substitution, $gp); // new GET-parameters
			$newgp = preg_replace('/^&/', '?', $newgp);
		}else{
			$s = ($gp) ? '&' : '?';
			$newgp = $gp . $s . $varname . '=' . $value;
		}

		$anchor = (isset($matches[3])) ? $matches[3] : '';
		return $matches[1] . $newgp . $anchor;
	}

	/**
	 * транслитерация строки
	 */
	public static function transliteration($str = ''){
		if(empty($str)) return $str;
		$abc = ['а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'j',
		        'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
		        'х' => 'x', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',];
		// $res = strtr($str, array_flip($abc)); // en->ru
		// $res = strtr($str, $abc); // ru->en

		// заменяем все пробелы и приводим строку внижнему регистру
		$str = trim(mb_strtolower($str, 'UTF-8'));

		$res = strtr($str, $abc); // транслитерация. Переменная $word получит значение 'prochee'
		$res = preg_replace('#[^a-z^\s^0-9]#', '', $res);    // убираем все не валидные символы
		$res = str_replace(' ', '_', $res);
		return $res;
	}

	/**
	 * запись в файл
	 */
	protected function wHtml($html = '', $prefix = ''){ file_put_contents($prefix . '_html.html', $html); }
}