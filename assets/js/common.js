$(document).ready(() => {

	$('body').on('submit', '#search', () => {
		searchWord = $('[name="searchWord"]').val();
		console.log('searchWord: ', searchWord);
		// return false;

		$.ajax({
			// async: true,
			url: '/ajax.php',
			type: 'POST',
			data: {
				word: $('[name="searchWord"]').val()
			},
			dataType: 'json',
			complete: (jqXHR, textStatus) => {
				// if (DEBUG) {
				console.log('textStatus:', textStatus);
				console.log('jqXHR:', jqXHR);
				// }
			},
			success: function (json) {
				console.log('json', json);
				$('#listResult').html(json.html);
				// return false;
			}
		});
		return false;
	});

});