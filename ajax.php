<?php

//error_reporting(E_ALL);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
require __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';

$json = [];
$json['html'] ='';
if(!Dump::isAjax()) exit(json_encode($json));

$word = $_POST['word'];
//$word = 'любой текст';
$ParserYa = new ParserYa;
$json['html'] = $ParserYa->run($word);
exit(json_encode($json));
